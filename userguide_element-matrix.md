# Element and Matrix: the quick guide to not losing your encrypted messages

## TL;DR (or read the [element user guide](element-user-guide.pdf))
If using encrypted rooms in Elements, it is recommended that you:
* **Activate Element's secure backup** option **as soon as possible**
  (under **Settings** > **Security & Privacy**).
* **[Generate and safely store](#backing-up-encryption-keys-in-elements) the secure backup's security key** - ideally in a password manager. This can only be done *once*. This **backup security key is essential** to get your encrypted messages back if you sign out (see below).
* **Do not sign out.** If you still have to, try to always stay signed-in in an Element session on **at least one device**. Note that being
  signed-in a session does not mean you must be online at all times: a device can be shutdown or
  offline, but the user should not "sign-out" of a session unless absolutely needed.
* Usage of the **"Export E2E room key"** functionality is optional, and, in most cases, not that
  useful.


<br>

## What is Matrix and Element
[Matrix](https://matrix.org) is an open source secure and decentralized communication protocol to
send messages or make call (VoIP). Matrix, however, is just a protocol, and in order to send or
receive messages/calls, it is necessary to use a so-called "client" application.

[Element](https://element.io) if a Matrix client, that uses the Matrix protocol to provide users
with a chat application having similar functionality to mattermost or slack (with e.g. different **rooms** to organize conversations by topic, possibility to send private messages, etc.).

In addition to standard chat, Element also allows the usage of
**secure end-to-end encrypted messaging**. This Encryption is **room specific**, and must be
enabled individually for each room. Encryption in Element is thus neither automatic nor
compulsory - one can e.g. keep public rooms unencrypted so that anyone can read their messages.


<br>

## Encryption in Element
In each **encrypted room**, messages sent by a given sender to a given recipient are encrypted
using symmetric encryption (the same key is used to encrypt and decrypt messages). This ensures
that only the intended recipients will be able to read the messages (e.g. if messages are
intercepted by a man-in-the-middle attack). It also means that,
**without the correct key, messages can no longer be decrypted**, even by the legitimate recipient
(if they lose their keys to decrypt messages).

An additional complication is that, in an effort to maximize security, Element uses so called
**[perfect forward secrecy - PFP](https://en.wikipedia.org/wiki/Forward_secrecy)**, a mechanism
in which encryption keys are regularly replaced/updated so that, if key is compromised, only
a limited amount of messages can be decrypted (note: this is a simplified description of PFP that
is not 100% accurate but good enough to illustrate the general principle).

In Elements, **encryption keys for each user and room are replaced/updated frequently** (every
few hours or so). As a result, being able to read the history of messages in Elements quickly
requires hundreds of keys, a number that only increases over time. These **keys are stored locally**
on the user's computer in an encrypted form, so that if an attacker has access to the computer,
they cannot get hold of the keys.

A last important aspect to be aware of is that the **encryption keys are session specific**. When
encryption keys are exchanged/updated between users, they are sent in an encrypted form, and can
only be decrypted by the Element session(s) for which they were intended.  
As will be detailed below, this has **important implications** since int means that if a user
signs-out of a given session or opens a session on a new computer, the message encryption keys that
were encrypted for a previous session can no longer be used because they can no longer be decrypted (unless a backup of the message encryption keys is made - more on that later).

#### Summary:
* In encrypted rooms, messages are **encrypted with keys that are rotated/updated regularly**.
* New/updated encryption keys are exchanged with other users via the Matrix server, but are
  encrypted with a key that is specific to the user's current session. In other words, encryption
  keys can **only be decrypted by the Element session(s) for which they were intended**. E.g.
  they cannot be decrypted by a future Element session.
* A sign-out / sign-in operation **creates a new session**. As a result, any key that has been
  sent in the time window when a user was signed-out cannot be recovered. The key is technically
  available, but cannot be decrypted since the decryption key is associated to the former session.
* Any **encrypted messages sent while a user is sign-out are therefore unreadable** (Note: some
  might still be readable, if their encryption key has not been rotated during the time the user
  was signed-out, but do not rely on this).
* To safeguard against loss of message encryption keys and session changes that could render your
  entire message history unreadable, it is of utmost importance to create a **backup of the room encryption keys**. This is explained in details in the next section of this guide.
* Without a backup of the keys, logging-in to a new Element session (e.g. on a new computer)
  **will not allow to read any past encrypted messages** (newly sent messages will be readable,
  however).

<br>

## Backing-up encryption keys in Elements
As outlined above, having access to current and past message encryption keys is essential in order
to be able to read the history of messages in encrypted rooms.
**Having a backup of the entire history of encryption keys is essential**, so that if something
goes wrong (lost laptop, corrupted hard-drive, etc.), all the past encrypted messages remain
readable.  
Fortunately, Elements make it easy to automatically back-up all message encryption keys on a
Matrix server. The keys backup is itself also encrypted.

**This automatic backup procedure should be enabled as soon as possible**, using the following
steps:
1. In the **Settings**, go to **Security & privacy** and look for the **Encryption** and
   **Secure backup** option. By default, the key auto-backup is not enabled, as shown below
   (note the "Your keys are not being backed up for this session"):

   ![backup_disabled](img/element_userguide_key_backup_1.png)

2. Click on **Set up**, then select **Generate a Security Key** and click **Continue**:

   ![setup_backup_1](img/element_userguide_key_backup_2.png)

3. A **security key** is generated for you. Store it somewhere safe, ideally in a password
   manager. Keeping this security key is very important, as it is required to get access to the
   backup of the message encryption keys. If the key is lost, the backup can no longer be read
   and all past message history becomes unreadable.

   After the key is safely stored, click on **Continue**.

4. Congrats, the secure backup is now up-and-running! (you should have a green checkbox with the
   message "This session is backing up your keys", as shown below).  
   Each time that a new message encryption key is received and decrypted by your current session,
   a copy of this key will now be uploaded to the online secure backup. These encryption keys can
   then be recovered at anytime, even if you switch to a new Element session, because they are now
   encrypted with your security key rather than with a session-specific key.

   ![setup_backup_complete](img/element_userguide_key_backup_4.png)


#### Summary:
* Enable **secure backup** of message encryption keys as soon as possible.
* Once secure backup is enabled, any new encryption key that has been retrieved and decrypted is
  automatically stored on the Matrix server. From this point on, backed-up keys can always be
  retrieved using the **security key**, which means that the history of encrypted messages will
  always be readable, even if using a different Element session (e.g. on a new computer).
* Backed-up keys become session-independent. The are available in all sessions, as long as these
  sessions are "verified" by entering your security key.
* The **security key must be stored safely**, as it is essential to get access to the backup.

**IMPORTANT:** unless there is a good reason to do so, **do not sign-out** of an Element session.
Any encrypted message sent while a user is signed-out will most likely be unreadable with
**no possibility of recovery** (unless the user has another Element session running in parallel
on a different machine - more on that in the next section).


<br>

## Switching to a new Element session (e.g. when using a new device)
Each time a user signs-in to Element, a new **session** is created. This has its importance since,
as explained above, the ability to decrypt message encryption keys is session-dependent. Note that
it is possible to have multiple sessions running in parallel on different devices (e.g. a computer
and a phone).

The following are actions that create **a new Element session**:
* Signing-out of the Element application on your device and signing-in again. This creates a new
  session and erases the old one.
* Installing Element on a different device and signing-in. This creates a new session that runs
  in parallel of any already existing session on another device.
* Deleting and re-installing Elements on your current device (e.g. if a computer is re-installed).
  This creates a new session and erases the old one.

The following actions **do not** create a new Element session:
* Quitting the Element application (or web-app) without signing-out.
* Shutting down and restarting the device that runs Element.

As long as a user keeps using the same device and does not sign-out of their Element session, that
session remains active and all messages - both read and unread (i.e. messages sent while the device
is shutdown or offline) - will be readable.

However, **if a new session is created and the old session is no longer available** (e.g. because
it was overwritten, the device was erased or lost, etc.), then messages that were sent before
the user signed-in to their new Element session, and that were not read in the old session will
be **permanently unreadable**, with no option of recovery.

Example of an unreadable message because the key to decrypt it is missing:
![element_unable_to_decrypt](img/element_userguide_unable_to_decrypt.png)

Here is a scenario to illustrate this:
* let's assume Bob is using an encrypted Element room to discuss things with Alice. Both Alice and
  Bob have enabled secure backup on their devices, and thus all of their message encryption
  keys are automatically backed-up in their respective accounts on the Matrix severs. Bob shuts
  down his computer every evening, but since he keeps using the same Element session, he is able to decrypt and read any message that Alice has sent while his computer was off.

* On a Monday morning, Bob's computer suddenly stops working and must be sent-in for repair. A few
  days later on Friday, Bob gets his computer back: the hard drive was faulty and had to be
  replaced.
  Bob re-installs Elements, logs-in to his user account and enters his security key. This allows
  him to recover all the message encryption keys for messages that he read before Monday morning,
  and he has thus full access to his chat history with Alice.

* However, Bob still faces a problem: any message sent by Alice after he last connected to Matrix
  on Monday morning are unreadable. The reason is that the keys to decrypt these messages are
  encrypted for his old Element session, and cannot be decrypted by his new Element session on his
  repaired computed. Bob has no way to recover these messages, and the best he can do at this point
  is to ask Alice to send them again.

One way that Bob could have been able to read (and gain permanent access to) the messages sent by
Alice between Monday morning and Friday is to have another element session running on a different
device. In this scenario, as soon as Bob realizes that his computer is (possibly) lost, he opens a
new Element session on an alternative device (e.g. his phone).  
Upon long-in, Element will suggest to "verify" the current session by entering the **security key**
associated with Bob's secure backup, as shown here:

![element_verify_account](img/elements_userguide_input_security_key.png)

If the correct security key is entered, the session is verified:

![element_account_is_verified](img/element_userguide_session_verified.png)

This that:
1. Bob has now access to his entire encrypted message history on his new device.
2. Any new encryption key used by Alice is now (also) encrypted for Bob's new Element session, to
   which he has access.
3. When the encryption keys are decrypted by his new session, they get backed-up in the online
   secure backup, ensuring that there is no gap in encryption key history in the backup.

When Bob then gets his computer back on Friday (with a new hard drive), he simply re-installs
Element, logs-in with his security key and automatically regains access to the entire message
history.  
At this point Bob has gone through the entire tragedy of losing his computer unharmed, and
has **not missed a single message from Alice**. He can now sign-out of Element on his secondary
device, if he wishes.

#### Summary:
* **Unread encrypted messages** sent to an old session
  **cannot be decrypted by a new Element session**, even if secure backup is enabled.
* Encrypted messages that are read in an Element session where secure backup is activated become
  **readable in any session**, because the encryption keys for these messages is stored in the
  online backup.
* Message encryption keys **stored in the online secure backup** are encrypted with the security
  key, and **are no longer bound to a specific session**. They can be read by any of your Element sessions.
* To avoid having unreadable messages, users must **avoid being singed-out of Element sessions**
  as much as possible. This means that:
   * Do not sign-out of an Element session on your device.
   * If you absolutely must sign-out of a session, you should sing-in again **as soon as possible**.
   * If a prolonged sing-out on a given device is unavoidable (e.g. the machine needs to be
     re-installed, the device is retired), sign-in to a new Element session on a different device
     **before** signing-out on your current device. In this way you will avoid any message
     becoming unreadable.
   * If access to a machine is lost (e.g. stolen computer), a new sign-in on a different device
     should be made as soon as possible.
   * It is important that **secure backup is always enabled** on all used devices.


<br>

## What about "Export E2E room keys" in the Encryption setting?
A supplementary backup option for message encryption keys is available in the form of an button
labeled **"Export E2E room keys"** under **Settings** > **Security & Privacy** > **Encryption** >
**Cryptography**.

![export_e2e_keys](img/element_userguide_exportE2E.png)

Clicking on this button **create an encrypted backup** of all message encryption keys in the form
of a file that is saved on the user's local machine. This file can later be imported in a new
session to restore the history of encryption keys, and gain access to messages up to the time point
at which the backup file was created.

While this is intrinsically doing the same as using **secure backup**, using "Export E2E room keys"
has several drawback and should therefore not be used as primary backup solution:
* To be useful, "Export E2E room keys" must be performed frequently. Using the automatic secure
  backup on the other had is set-and-forget.
* Since the  encrypted backup created by "Export E2E room keys" is a local file, it must itself
  also be backed-up somewhere else, otherwise it becomes useless if the device is stolen or
  becomes unusable (e.g. corrupted hard drive).

#### Summary:
* Using "Export E2E room keys" should not be used as a replacement for secure backup. It might
  possibly be used in addition to secure backup, for people who want an additional, offline, backup.
