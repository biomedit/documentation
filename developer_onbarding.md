[[_TOC_]]

## Git

### configuration

Add the following git configuration settings to your project (or set it globally with the additional `--global` option):

```
git config --add pull.rebase true
git config --add pull.ff only

# show the git configuration
git config --list
# ...
# pull.ff=only
# pull.rebase=true
```

Not really necessary, but nice addition: `git lg` to get a nicely rendered and coloured `git log` alternative:

```
git config --global alias.lg "log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
```


### Commit conventions:

In order to keep the Git history of our repos as clean and readable as possible, the following commit etiquette
is used:

- Changes to the code base are developed on a _feature branch_ before being merged into `dev`.
- When making changes to the code base (e.g. adding a new feature), use as few commits as possible,
  but as many as necessary. In other words, changes that go together should be part of a single commit, but
  changes that are unrelated should be in separate commits.
- Follow the commit message guidelines as defined in the `CONTRIBUTING.md` file of each project.
- Feature branches should be rebased (on `dev`) before they are merged, so that the merge is _fast-forward_
  and does not introduce an additional merge commit. In other words, if a feature branch is not rooted on
  `dev`, you should `git rebase` (and _force push_) before doing `git merge` in feature branches.
- Typical working pattern:
   - in gitlab, pick an issue, assign yourself and click on **Create merge request** to automatically create a branch 
   - `git fetch`
   - your newly created branch should be displayed. Go for it!
   - `git checkout 504-xxxx`
   - make commits to your feature
   - after some time, you might want to `rebase` on `dev` branch, since it might have changed in the meantime
   - `git fetch`
   - `git rebase dev`
   - once you've finished your feature, squash your commits
   - `git rebase -i dev`
   - replace all `pick` in front of the smaller commits with `s` or `squash`
   - force push your changes to the repository
   - `git push --force`
   - put a note in MR channel so somebody can pick up your MR and review it

### Useful links

- [Merging vs. Rebasing](https://www.atlassian.com/git/tutorials/merging-vs-rebasing)
- [fixup and autosquash](https://blog.sebastian-daschner.com/entries/git-commit-fixup-autosquash)

## GitLab

We use **GitLab** functionalities as much as possible. Our typical workflow looks as following:

1. Assign yourself to an issue in **GitLab** (to let people know that you are taking core of it).
1. Create a new `MR` in **GitLab**. This creates a new branch (tip: you can give a custom name to the branch
   by using the drop-down menu on the _create merge request_ button in **GitLab**).
1. In your local repository, fetch changes and switch to the newly created branch for the `MR`:

   ```bash
   git fetch
   git switch <MR branch>
   ```

1. When your changes/feature are/is ready, push changes to remote. If your `MR` branch is no longer rooted on
   the latest commit of `dev`, please **rebase** and **force push** your changes.
1. In **GitLab**, mark the `MR` as ready.
1. In the **MR channel** of **matrix/element**, post a message indicating your `MR` is ready for review -
   please include a link to the `MR` in **GitLab** in your message.
   Example: `**sett** - [new feature description](https://gitlab.com/...)`.
1. Another person should now review the `MR`. To indicate that they will review a `MR`, a person should:
   - React to the message in **matrix/element** with an emoji.
   - Assign themselves as reviewer in the **GitLab** `MR`.
1. When all the threads are resolved, the reviewer typically approves the `MR`.
1. Merge the `MR` via **GitLab** interface. You can then delete your local MR branch if you wish to:

   ```bash
   git fetch --prune
   git branch -d <MR branch>
   ```

## Glossary

![container](img/containers.png)

### Beats

[Beats](https://www.elastic.co/beats/) are a collection of lightweight log/data shippers acting as agents installed on different servers. **Beats** send data directly to **OpenSearch** (see below) or via [Logstash](https://github.com/elastic/logstash).

Currently we are using following **Beats**:

- [filebeat](https://www.elastic.co/beats/filebeat): lightweight shipper for logs. **Docker** container logs of **BIWG** running applications/services are collected by **filebeat** and sent to **OpenSearch**.
- [heartbeat](https://www.elastic.co/beats/heartbeat): lightweight shipper for uptime monitoring. **BIWG** applications/services are monitored by **heartbeat** which sends defined uptime metrics, either directly to **OpenSearch** or to **Logstash** for enrichment before indexing.

**filebeat** specific configuration are be found in **portal** and in the service repositories at the following path: `/services/filebeat/`. Integration is done via **Docker Compose**.

### Celery

[Celery](https://docs.celeryproject.org/en/stable/) is a powerful asynchronous job queue used for running tasks in the background, integrated in **Django** and works in combination with with a message broker like **Redis** (see below). Functions or methods known to take some time to complete are decorated with a `@shared_task` decorator. A message is then sent to the **Redis** message broker, with all the necessary information about the task. A Celery worker then picks up the task and works on it. Once the job is done, the celery worker reports back to the message broker **Redis** which again reports back to the application. Celery workers usually register to the message broker.

Tools like [Flower](https://flower.readthedocs.io/en/latest/index.html) can help monitoring these asynchronous tasks.

**Celery** can also run periodic tasks defined in `/backend/admin/` in the **Periodic tasks** table. You will find **Celery** configuration in **portal** in [settings.py](https://gitlab.com/biomedit/portal/-/blob/dev/web/portal/settings.py).

### Depfu

Each week [Depfu](https://depfu.com/) creates a `MR` in **next-widgets** and **portal**. This `MR` updates the _npm_ dependencies. Like any other `MR` it needs reviewing.

### Mailhog

[Mailhog](https://github.com/mailhog/MailHog) is an email catcher used on [the staging portal](https://portal-staging.dcc.sib.swiss/).

Mailhog intercepts emails that would normally be sent by the staging portal, and redirects them to the **Emails channel**
of matrix/elements instead. In this way we do not spam people unnecessarily, and have all emails grouped together in a
matrix channel.

### OpenSearch

Our alerting system is based on **OpenSearch**:

- [OpenSearch](https://opensearch.org/) is a community-driven, open source search and analytics suite derived
  from Apache 2.0 licensed **Elasticsearch** _7.10.2_ & **Kibana** _7.10.2_.
- Our OpenSearch instance is [hosted here](https://fl-10-93.zhdk.cloud.switch.ch/app/login?nextUrl=%2Fapp%2Fdiscover#/).

**OpenSearch** sends relevant alerts, or log entries identified as such, to the channel _Alerts_ in **matrix/element**.

To lookup an alert in the _Alert_ channel:
- log into our [OpenSearch instance](https://fl-10-93.zhdk.cloud.switch.ch/app/login?nextUrl=%2Fapp%2Fdiscover#/)
- click on the hamburger symbol (top-left) **≡**
- choose **Discover**
- choose **logstash-*** from the dropdown menu (for most cases, unless there is some sort of heartbeat error)
- enter the value of the `correlationId` reported by the _Alert_ channel into the search field
- make sure to extend the time range by clicking on the calendar symbol right next to it

### Redis

[Redis](https://redis.io/) is an in-memory data structure store, used as a database, cache, and message broker. It is used by **Celery** (see above).

### Snyk

[Snyk](https://snyk.io/) is a product to find and automatically fix vulnerabilities in dependencies. **Snyk** seems to be more complete than **Depfu**. Back then, when looking for an automatic dependencies management system, **Snyk** was not supporting **GitLab** projects (and **Depfu** was).

An account for **BIWG** has been setup. We are currently monitoring **next-widgets** only (kindly refer to **Snyk** [project](https://app.snyk.io/org/biomedit/project/f5665d8f-ce19-4efe-a32c-dbac9d840042) - you will need to be part of the **BIWG** organization at **Snyk** place though).

### Traefik

[traefix](https://doc.traefik.io/traefik/) is a HTTP reverse proxy and load balancer particularly suitable for dynamic environments: it supports service discovery, SSL management, metrics and powerful integration with, amongst others, **Docker** and **Kubernetes**.

**Traefik** specific configuration can be found at following path `/services/traefik/`. Integration is realized via **Docker Compose**.
